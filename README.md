# My build of dmenu

## Patches

- [Grid](https://tools.suckless.org/dmenu/patches/grid/): Adds the option to configure columns.
- [Fuzzy Match](https://tools.suckless.org/dmenu/patches/fuzzymatch/): Adds support for fuzzy-matching, allowing to type non-consecutive portions of the string to be matched.
- [Fuzzy Highlight](https://tools.suckless.org/dmenu/patches/fuzzyhighlight/): Makes it so that fuzzy matches gets highlighted.
- [Case Insensitive](https://tools.suckless.org/dmenu/patches/case-insensitive/): Changes case insensitive matching to default behaviour.
- [Password](https://tools.suckless.org/dmenu/patches/password/): Adds the option -P to replace keyboard input with dots.


## Installation

```bash
git clone https://gitlab.com/erzh08/dmenu.git
cd dmenu
sudo make install
```
